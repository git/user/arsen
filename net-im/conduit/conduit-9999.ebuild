# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Autogenerated by pycargoebuild 0.13.2

EAPI=8

inherit cargo systemd

DESCRIPTION="A Matrix homeserver written in Rust"
HOMEPAGE="https://conduit.rs"

if [[ ${PV} = 9999 ]]; then
	EGIT_REPO_URI="https://gitlab.com/famedly/${PN}.git"
	inherit git-r3
else
	SRC_URI="
		https://gitlab.com/famedly/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2
		${CARGO_CRATE_URIS}
	"
	KEYWORDS="~amd64 ~arm64"
	S="${WORKDIR}/${PN}-v${PV}"
fi

LICENSE="Apache-2.0"
# Manually inspected crate licenses
LICENSE+="
	ISC openssl MIT
"
# Dependent crate licenses
LICENSE+="
	Apache-2.0 BSD-2 BSD CC0-1.0 ISC MIT MPL-2.0 Unicode-DFS-2016 ZLIB
"
SLOT="0"

IUSE="+rocksdb"

BDEPEND="
	virtual/rust
	rocksdb? (
		virtual/pkgconfig
		sys-devel/clang
	)
"
DEPEND="
	rocksdb? ( dev-libs/rocksdb )
"
RDEPEND="${DEPEND}"

src_unpack() {
	if [[ ${PV} = 9999 ]]; then
		git-r3_src_unpack
		cargo_live_src_unpack
	else
		cargo_src_unpack
	fi
}

src_configure() {
	# See https://wiki.gentoo.org/wiki/Writing_Rust_ebuilds#Unbundling_C_libraries
	local ld="${ESYSROOT}/usr/$(get_libdir)"
	export PKG_CONFIG_ALLOW_CROSS=1

	# This mess seems to be required based on librocksdb-sys' build.rs.
	export ROCKSDB_LIB_DIR="${ld}"
	export SNAPPY_LIB_DIR="${ld}"
	export LZ4_LIB_DIR="${ld}"
	export Z_LIB_DIR="${ld}"
	export BZ2_LIB_DIR="${ld}"
	export ZSTD_LIB_DIR="${ld}"
	export ZSTD_SYS_USE_PKG_CONFIG=1

	local myfeatures=(
		backend_sqlite
		systemd
		conduit_bin
		persy
		$(usev rocksdb backend_rocksdb)
	)
	cargo_src_configure --no-default-features
}

src_install() {
	local target_dir="$(usex debug debug release)"
	dobin target/"${target_dir}"/conduit

	systemd_dounit "${FILESDIR}"/matrix-conduit.service
	dodoc README.md LICENSE conduit-example.toml
}
